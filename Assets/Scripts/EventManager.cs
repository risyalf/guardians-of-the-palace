﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static bool isSuccessEvent_1;
    public static bool isSuccessEvent_2;
    public static bool isSuccessEvent_3;

    public static bool isFinal;

    private void Start()
    {
        isSuccessEvent_1 = true;
        isSuccessEvent_2 = true;
        isSuccessEvent_3 = true;
    }

    public int countTheWin()
    {
        if (isSuccessEvent_1 && isSuccessEvent_2 && isSuccessEvent_3)
        {
            return 3;
        }

        else if (isSuccessEvent_1 && isSuccessEvent_2 || isSuccessEvent_1 && isSuccessEvent_3 || isSuccessEvent_2 && isSuccessEvent_3)
        {
            return 2;
        }

        else
        {
            return 1;
        }


    }
}
