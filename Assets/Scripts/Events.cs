﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Events : MonoBehaviour
{
    public bool playingEvent_1;
    public bool playingEvent_2;
    public bool playingEvent_3;
    public bool playingEventFinal;

    public UnityEvent event_1;
    public UnityEvent event_2;
    public UnityEvent event_3;
    public UnityEvent eventFinal;

    private void Update()
    {
        if (playingEvent_1 && Dialogue.canPlaySentence)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                event_1.Invoke();
            }
        }

        if (playingEvent_2 && Dialogue.canPlaySentence)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                event_2.Invoke();
            }
        }

        if (playingEvent_3 && Dialogue.canPlaySentence)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                event_3.Invoke();
            }
        }

        if (playingEventFinal && Dialogue.canPlaySentence)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                eventFinal.Invoke();
            }
        }
    }
}
