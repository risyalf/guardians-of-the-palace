﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform player;

    public bool followPlayer;

    private void Start()
    {
        followPlayer = true;
    }

    private void Update()
    {
        if(followPlayer)
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
    }

    IEnumerator CameraShaking()
    {
        for (int i = 0; i < 10; i++)
        {
            float rndm = Random.Range(-0.25f, 0.25f);
            transform.position = new Vector3(transform.position.x + rndm, transform.position.y + rndm, transform.position.z);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void CameraEffect_Shaking()
    {
        StartCoroutine(CameraShaking());
    }
}
