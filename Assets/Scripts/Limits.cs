﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Limits : MonoBehaviour
{
    public List<Transform> limitCollider = new List<Transform>();

    private void Start()
    {
        GettingReady();
    }

    private void GettingReady()
    {
        foreach (Transform limit in GetComponentsInChildren<Transform>())
        {
            if (limit.name != this.gameObject.name)
            {
                limitCollider.Add(limit);
                limit.gameObject.SetActive(false);
            }
        }

        foreach (Transform limit in limitCollider)
        {
            if (limit.name.Contains("Trigger"))
            {
                limit.gameObject.SetActive(true);
            }
        }
    }
}
