﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_2 : EventBase
{
    bool playFirstAnimation;

    private void Start()
    {
        playFirstAnimation = true;
    }

    public void PlayEvent_2()
    {
        StartCoroutine(EventFlow());
    }

    IEnumerator EventFlow()
    {
        playerControl.EventIsStarting(true);

        if (playFirstAnimation)
        {
            animations.PlayTheAnimation(2, 0, "GuidanceCharacter_FadeIn");

            yield return new WaitForSeconds(1);

            playFirstAnimation = false;
        }

        dialogueEvent.PlayingDialogue();

        if (!dialogueEvent.isPlayingDialogue)
        {
            triggerEvent.SetActive(false);

            animations.PlayTheAnimation(2, 0, "GuidanceCharacter_FadeOut");

            yield return new WaitForSeconds(1);

            playerControl.EventIsStarting(false);
        }
    }
}