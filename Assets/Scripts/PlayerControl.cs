﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    private Rigidbody2D _myRigidBody;
    [SerializeField]
    private float _speed;
    private float _saveSpeed;

    private void Start()
    {
        _myRigidBody = GetComponent<Rigidbody2D>();
        _saveSpeed = _speed;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            _myRigidBody.velocity = Vector2.up * _speed;
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            _myRigidBody.velocity = Vector2.down * _speed;
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            _myRigidBody.velocity = Vector2.left * _speed;
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _myRigidBody.velocity = Vector2.right * _speed;
        }

        if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
        {
            _myRigidBody.velocity = Vector2.zero;
        }
    }

    public void EventIsStarting(bool isStarting)
    {
        if (isStarting)
        {
            _speed = 0;
        }

        else if (!isStarting)
        {
            _speed = _saveSpeed;
        }
    }
}
