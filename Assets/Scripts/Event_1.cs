﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_1 : EventBase
{
    bool playFirstAnimation;

    private void Start()
    {
        playFirstAnimation = true;

        trueEvent.SetActive(false);
        falseEvent.SetActive(false);
        limitEvent.SetActive(false);
    }

    public void PlayEvent_1()
    {
        StartCoroutine(EventFlow());
    }

    IEnumerator EventFlow()
    {
        playerControl.EventIsStarting(true);

        if (playFirstAnimation)
        {
            animations.PlayTheAnimation(1, 0, "GuidanceCharacter_FadeIn");

            yield return new WaitForSeconds(1);

            playFirstAnimation = false;
        }

        dialogueEvent.PlayingDialogue();

        if (!dialogueEvent.isPlayingDialogue)
        {
            triggerEvent.SetActive(false);

            trueEvent.SetActive(true);
            falseEvent.SetActive(true);
            
            animations.PlayTheAnimation(1, 0, "GuidanceCharacter_FadeOut");

            yield return new WaitForSeconds(1);

            playerControl.EventIsStarting(false);
        }
    }
}
