﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public Animator[] animators;

    private void Start()
    {
        Preparation();
    }

    private void Preparation()
    {
        /*foreach(Animator anim in animators)
        {
            anim.gameObject.SetActive(false);
        }*/

        animators[0].gameObject.SetActive(false);
    }

    public void PlayTheAnimation(int doEvent, int index, string animationName)
    {
        animators[index].gameObject.SetActive(true);
        animators[index].Play(animationName);

        /*if (doEvent == 1)
        {
            transform.position = new Vector3(transform.position.x, 7, transform.position.z);
        }

        if (doEvent == 2)
        {
            transform.position = new Vector3(transform.position.x, 16, transform.position.z);
        }

        if (doEvent == 3)
        {
            transform.position = new Vector3(transform.position.x, 37, transform.position.z);
        }

        if (doEvent == 3)
        {
            transform.position = new Vector3(transform.position.x, 55, transform.position.z);
        }*/

        switch (doEvent)
        {
            case 1:
                transform.position = new Vector3(transform.position.x, 7, transform.position.z);
                break;
            case 2:
                transform.position = new Vector3(transform.position.x, 16.45f, transform.position.z);
                break;
            case 3:
                transform.position = new Vector3(transform.position.x, 39.5f, transform.position.z);
                break;
            case 4:
                transform.position = new Vector3(transform.position.x, 55.44f, transform.position.z);
                break;
            default:
                break;
        }
    }

    public void FinalAnimation()
    {
        CameraControl cameraControl = GameObject.Find("Main Camera").GetComponent<CameraControl>();
        PlayerControl playerControl = GameObject.Find("Player").GetComponent<PlayerControl>();

        cameraControl.followPlayer = false;
        cameraControl.transform.position = Vector3.Lerp(cameraControl.transform.position, new Vector3(-100, 57.5f, cameraControl.transform.position.z), 1f);

        playerControl.transform.position = Vector3.Lerp(playerControl.transform.position, new Vector3(-100, 57f, playerControl.transform.position.z), 1);
    }
}
