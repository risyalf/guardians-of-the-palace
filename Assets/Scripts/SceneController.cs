﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class SceneController : MonoBehaviour, IPointerClickHandler
{
    public void GoToMainScene()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(this.gameObject.name == "FinalPanel")
            SceneManager.LoadScene("MainMenu");
    }
}
