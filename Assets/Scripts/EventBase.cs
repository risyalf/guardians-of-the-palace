﻿using UnityEngine;

public class EventBase : MonoBehaviour
{
    public static bool startEvent;

    public AnimationController animations;
    public PlayerControl playerControl;

    public Dialogue dialogueEvent;

    public GameObject triggerEvent;
    public GameObject trueEvent;
    public GameObject falseEvent;
    public GameObject limitEvent;
}
