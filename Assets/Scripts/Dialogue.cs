﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public static bool canPlaySentence;
    public static bool isNewDialogue;

    public GameObject textPanel;
    public Text text;
    public int index;
    public string[] sentences;

    public bool isPlayingDialogue;

    // private bool _nextSentences;

    private void Start()
    {
        canPlaySentence = true;
        // _nextSentences = true;
        textPanel.SetActive(false);
    }

    private void Update()
    {
        /*if (Input.GetKey(KeyCode.Space))
        {
            _nextSentences = true;
        }

        if (_nextSentences)
        {
            StartCoroutine(PlayDialogue(0.05f));

            _nextSentences = false;
        }*/
    }

    IEnumerator PlayDialogue(float speed)
    {
        if (isNewDialogue)
        {
            index = 0;
            isNewDialogue = false;
        }

        textPanel.SetActive(true);
        isPlayingDialogue = true;

        if (index >= sentences.Length)
        {
            isPlayingDialogue = false;
            textPanel.SetActive(false);
        }

        if (index < sentences.Length && canPlaySentence)
        {
            canPlaySentence = false;
            text.text = "";
            foreach (char letter in sentences[index].ToCharArray())
            {
                text.text += letter.ToString();
                yield return new WaitForSeconds(speed);
            }

            canPlaySentence = true;
        }

        index++;
    }

    public void PlayingDialogue()
    {
        StartCoroutine(PlayDialogue(0.01f));
    }
}
