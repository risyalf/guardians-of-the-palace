﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Event_3 : EventBase
{
    public GameObject properties;
    public Button killButton;
    public Button liveButton;

    CameraControl cameraControl;

    bool playFirstAnimation;

    private void Start()
    {
        cameraControl = GameObject.Find("Main Camera").GetComponent<CameraControl>();

        playFirstAnimation = true;

        properties.SetActive(false);

        /*trueEvent.SetActive(false);
        falseEvent.SetActive(false);
        limitEvent.SetActive(false);*/
    }

    public void PlayEvent_3()
    {
        StartCoroutine(EventFlow());
    }

    IEnumerator EventFlow()
    {
        playerControl.EventIsStarting(true);

        if (playFirstAnimation)
        {
            animations.PlayTheAnimation(3, 0, "GuidanceCharacter_FadeIn");

            yield return new WaitForSeconds(1);

            playFirstAnimation = false;
        }

        dialogueEvent.PlayingDialogue();

        if (!dialogueEvent.isPlayingDialogue)
        {
            triggerEvent.SetActive(false);

            animations.PlayTheAnimation(3, 0, "GuidanceCharacter_FadeOut");

            yield return new WaitForSeconds(1);

            properties.SetActive(true);

            killButton.onClick.AddListener(OnClickFalse);
            liveButton.onClick.AddListener(OnClickTrue);
        }
    }

    void OnClickFalse()
    {
        EventManager.isSuccessEvent_3 = false;

        properties.SetActive(false);

        animations.PlayTheAnimation(0, 1, "KillHim");

        cameraControl.CameraEffect_Shaking();

        playerControl.EventIsStarting(false);
    }

    void OnClickTrue()
    {
        EventManager.isSuccessEvent_3 = true;

        properties.SetActive(false);

        playerControl.EventIsStarting(false);
    }
}
