﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    public CameraControl camera;
    public Events eventController;

    private CameraControl _cameraControl;
    private Limits _limits;

    private void Start()
    {
        _cameraControl = FindObjectOfType<CameraControl>();
        _limits = FindObjectOfType<Limits>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /// <summary>
        /// Mulai Event 1
        /// </summary>

        if (collision.gameObject.name == "TriggerEvent_1")
        {
            Dialogue.isNewDialogue = true;
            eventController.playingEvent_1 = true;
            eventController.event_1.Invoke();
        }

        if (collision.gameObject.name == "TrueEvent_1")
        {
            _cameraControl.CameraEffect_Shaking();
            collision.gameObject.SetActive(false);
            FindObjectOfType<Event_1>().falseEvent.SetActive(false);
            FindObjectOfType<Event_1>().limitEvent.SetActive(true);
            EventManager.isSuccessEvent_1 = true;
            eventController.playingEvent_1 = false;
        }

        if (collision.gameObject.name == "FalseEvent_1")
        {
            _cameraControl.CameraEffect_Shaking();
            collision.gameObject.SetActive(false);
            FindObjectOfType<Event_1>().trueEvent.SetActive(false);
            FindObjectOfType<Event_1>().limitEvent.SetActive(true);
            EventManager.isSuccessEvent_1 = false;
            eventController.playingEvent_1 = false;
        }

        /// <summary>
        /// Akhir Event 1
        /// </summary>


        /// <summary>
        /// Setelah Event 1, mentrigger limit 1
        /// </summary>

        if (collision.gameObject.name == "TriggerLimit_1")
        {
            foreach (Transform limit in _limits.limitCollider)
            {
                if (limit.gameObject.name.Contains("Limit_1"))
                    limit.gameObject.SetActive(true);
            }

            collision.gameObject.SetActive(false);
        }

        /// <summary>
        /// Limit 1 sudah trigger
        /// </summary>


        /// <summary>
        /// Mulai Event 2
        /// </summary>

        if (collision.gameObject.name == "TriggerEvent_2")
        {
            Dialogue.isNewDialogue = true;
            eventController.playingEvent_2 = true;
            eventController.event_2.Invoke();
        }

        if (collision.gameObject.name.Contains("FalseEvent_2"))
        {
            print("Anda Salah");
            GameObject.Find("FalseEvent_2").SetActive(false);
            EventManager.isSuccessEvent_2 = false;
        }

        /// <summary>
        /// Akhir Event 2
        /// </summary>


        /// <summary>
        /// Setelah Event 2, mentrigger limit 2
        /// </summary>

        if (collision.gameObject.name == "TriggerLimit_2")
        {
            _cameraControl.CameraEffect_Shaking();

            foreach (Transform limit in _limits.limitCollider)
            {
                if (limit.gameObject.name.Contains("Limit_2"))
                    limit.gameObject.SetActive(true);
            }

            eventController.playingEvent_2 = false;
            collision.gameObject.SetActive(false);
        }

        /// <summary>
        /// Limit 2 sudah trigger
        /// </summary>


        /// <summary>
        /// Mulai event 3
        /// </summary>

        if (collision.gameObject.name == "TriggerEvent_3")
        {
            Dialogue.isNewDialogue = true;
            eventController.playingEvent_3 = true;
            eventController.event_3.Invoke();
        }

        /// <summary>
        /// Akhir event 3
        /// </summary>


        /// <summary>
        /// Setelah Event 3, mentrigger limit 3
        /// </summary>

        if (collision.gameObject.name == "TriggerLimit_3")
        {
            _cameraControl.CameraEffect_Shaking();

            foreach (Transform limit in _limits.limitCollider)
            {
                if (limit.gameObject.name.Contains("Limit_3"))
                    limit.gameObject.SetActive(true);
            }

            eventController.playingEvent_3 = false;
            collision.gameObject.SetActive(false);
        }

        /// <summary>
        /// Limit 3 sudah tertrigger
        /// </summary>


        /// <summary>
        /// Mulai final event
        /// </summary>


        if (collision.gameObject.name == "TriggerEventFinal")
        {
            Dialogue.isNewDialogue = true;
            eventController.playingEventFinal = true;
            eventController.eventFinal.Invoke();
        }


        /// <summary>
        /// Akhir final event
        /// </summary>
    }
}
