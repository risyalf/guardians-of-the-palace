﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_Final : EventBase
{
    public GameObject[] charactersEvent;
    public GameObject[] finalScenes;

    public Dialogue badEndingDialogue;
    public Dialogue moderateEndingDialogue;
    public Dialogue goodEndingDialogue;

    EventManager eventManager;

    bool isFinalEventPlaying;
    bool playFirstAnimation;

    private void Start()
    {
        eventManager = GameObject.Find("EventsManager").GetComponent<EventManager>();

        Preparation();

        isFinalEventPlaying = false;
        playFirstAnimation = true;
    }

    void Preparation()
    {
        foreach(GameObject character in charactersEvent)
        {
            character.SetActive(false);            
        }

        foreach (GameObject scene in finalScenes)
        {
            scene.SetActive(false);
        }
    }

    public void PlayEventFinal()
    {
        if (!isFinalEventPlaying)
        {
            StartCoroutine(EventFlow());
        }

        if (isFinalEventPlaying)
        {
            StartCoroutine(FinalEvent());
        }
    }

    IEnumerator EventFlow()
    {
        playerControl.EventIsStarting(true);

        if (playFirstAnimation)
        {
            animations.PlayTheAnimation(4, 0, "GuidanceCharacter_FadeIn");

            yield return new WaitForSeconds(1);

            playFirstAnimation = false;
        }

        dialogueEvent.PlayingDialogue();

        if (!dialogueEvent.isPlayingDialogue)
        {
            triggerEvent.SetActive(false);

            animations.PlayTheAnimation(4, 0, "GuidanceCharacter_FadeOut");

            yield return new WaitForSeconds(1);

            print(eventManager.countTheWin());

            isFinalEventPlaying = true;

            StartCoroutine(FinalEvent());

            // playerControl.EventIsStarting(false);
        }
    }

    IEnumerator FinalEvent()
    {
        animations.FinalAnimation();

        if (eventManager.countTheWin() == 3)
        {
            // good ending
            charactersEvent[2].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            goodEndingDialogue.PlayingDialogue();

            if (!goodEndingDialogue.isPlayingDialogue)
            {
                finalScenes[0].SetActive(true);
                finalScenes[3].SetActive(true);
            }
        }

        else if (eventManager.countTheWin() == 2)
        {
            // moderate ending
            charactersEvent[1].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            moderateEndingDialogue.PlayingDialogue();

            if (!moderateEndingDialogue.isPlayingDialogue)
            {
                finalScenes[0].SetActive(true);
                finalScenes[2].SetActive(true);
            }
        }

        else
        {
            // bad ending
            charactersEvent[0].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            badEndingDialogue.PlayingDialogue();

            if (!badEndingDialogue.isPlayingDialogue)
            {
                finalScenes[0].SetActive(true);
                finalScenes[1].SetActive(true);
            }
        }
    }
}
